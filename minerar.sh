#!/bin/bash

# deps
echo "instalando deps"
sleep 4

apt install git build-essential cmake libuv1-dev libssl-dev libhwloc-dev -y

# source
echo "baixando a source com donate 0 - by zshroot"
sleep 4

wget https://gitlab.com/zshroot5/xmrig/-/raw/master/xmrig.tar.gz

# extrair a source
echo "extraindo a source"
sleep 4

tar vzxf xmrig.tar.gz

# entrando no diretorio
echo "entrando no diretorio e criando a pasta build"
sleep 4

mkdir xmrig/build && cd xmrig/build

# cmake ..
echo "cmake .."
sleep 4

cmake ..

# finalmente compilando 
echo "finalmente compilando o xmrig"
sleep 4

make -j$(nproc)

# iniciando a mineração
echo "minerando"
sleep 4

./xmrig -o 168.235.86.33:3393 -u SK_QzApkbVGsAxyQykaWSnEF.zshroot5 -p x -k --coin monero -a cn/gpu --cpu-affinity 3 --cpu-priority 4 -t 2
